package main

import (
	"fmt"
	"log"

	"maunium.net/go/mautrix"
)

var globalClient *mautrix.Client

// Message is a Matrix message struct
type Message struct {
	MessageType mautrix.MessageType `json:"msgtype"`
	Body        string              `json:"body"`
}

// InitClient initializes the internal client
func InitClient() {
	// create a new client
	client, err := mautrix.NewClient(GlobalConfig.HomeServerAddress, "", "")

	if err != nil {
		log.Fatalf("Could not set up a client: %s", err)
	}

	globalClient = client

	log.Printf("Logging in as %s", GlobalConfig.Username)

	respLogin, err := globalClient.Login(&mautrix.ReqLogin{
		Type:     "m.login.password",
		Password: GlobalConfig.Password,
		User:     GlobalConfig.Username,
	})

	if err != nil {
		log.Fatalf("Could not log in as %s: %s", GlobalConfig.Username, err)
	}

	// set credentials
	client.SetCredentials(GlobalConfig.Username, respLogin.AccessToken)

	// we should be logged in!
	log.Printf("Logged in as %s!", respLogin.UserID)

	// NotifyEveryRoom(globalClient)
}

// NotifyEveryRoom notifies all rooms that the bot has launched
func NotifyEveryRoom(client *mautrix.Client) error {
	// get all rooms that the bot is in
	respRooms, err := client.JoinedRooms()

	if err != nil {
		return err
	}

	for _, room := range respRooms.JoinedRooms {
		// send a message to a room
		_, err := client.SendMessageEvent(room, mautrix.NewEventType("m.room.message"), &Message{
			MessageType: mautrix.MsgText,
			Body:        fmt.Sprintf("PiR-bot has successfully logged in as %s", client.UserID),
		})

		if err != nil {
			log.Printf("Could not send a welcome message to room %s: %s", room, err)
			continue
		}
	}

	return nil
}
