package main

import (
	"log"
)

// GlobalConfig is a global configuration struct
var GlobalConfig Config

func main() {
	// notify the user
	log.Println("Welcome to PiR-bot")

	// load the config
	LoadDefaultConfig()

	GlobalConfig.InitConfig()
	GlobalConfig.LoadConfigs()

	// start up Matrix
	InitClient()
}
