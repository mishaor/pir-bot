module gitlab.com/mishaor/pir-bot

go 1.12

require (
	github.com/imdario/mergo v0.3.8
	github.com/jessevdk/go-assets v0.0.0-20160921144138-4f4301a06e15
	github.com/jessevdk/go-assets-builder v0.0.0-20130903091706-b8483521738f // indirect
	github.com/jessevdk/go-flags v1.4.0 // indirect
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/stretchr/testify v1.4.0 // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
	maunium.net/go/mautrix v0.1.0-alpha.3
)
