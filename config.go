package main

import (
	"encoding/json"
	"io/ioutil"
	"log"

	"github.com/imdario/mergo"
	"github.com/jinzhu/copier"
)

// paths to config files
var configFiles = []string{
	"/etc/pir-bot/config.json",
	"./config.json",
}

// Config is self-explanatory
type Config struct {
	Username          string `json:"username"`
	Password          string `json:"password"`
	HomeServerAddress string `json:"homeServerAddress"`

	InstanceDirectory string `json:"instanceDir"`
}

// ReadConfigIntoRaw reads and deserializes a given config
func ReadConfigIntoRaw(pointer *Config, configFile []byte) error {
	// unmarshal the configuration into a config struct
	err := json.Unmarshal(configFile, &pointer)

	if err != nil {
		return err
	}

	return nil
}

var defaultConfig Config // default config

// LoadDefaultConfig loads the default config internally
// so that it can be used later
func LoadDefaultConfig() {
	// open the default config
	// FIXME(govnokod): find a way to make Go not complain
	// about assets being missing as they're usually presentru
	// at compile time due to being built by go-assets-generator
	config, err := Assets.Open("/config.json")

	if err != nil {
		log.Panicf("Default config cannot be loaded: %s", err)
	}

	// read the default config
	configContents, err := ioutil.ReadAll(config)

	if err != nil {
		log.Panicf("Default config cannot be read: %s", err)
	}

	// parse the config
	err = ReadConfigIntoRaw(&defaultConfig, configContents)

	if err != nil {
		log.Panicf("Default config is corrupt (SOMETHING IS VERY WRONG WITH YOUR BINARY!): %s", err)
	}
}

// InitConfig initializes a config struct
func (config *Config) InitConfig() {
	// load the default config into a config struct
	copier.Copy(config, defaultConfig)
}

// ApplyConfig applies a configuration file to a global configuration
func (config *Config) ApplyConfig(configFile []byte) error {
	// load the given config
	var givenConfig Config
	err := ReadConfigIntoRaw(&givenConfig, configFile)

	if err != nil {
		return err
	}

	// merge the given config with the global config
	err = mergo.Merge(config, givenConfig, mergo.WithOverride)

	if err != nil {
		return err
	}

	return nil
}

// LoadConfigs tries to guess where the config is,
// if it fails to guess, it returns an error
func (config *Config) LoadConfigs() {
	log.Println("Looking for configuration files")

	for _, filename := range configFiles {
		// open the file
		log.Printf("Opening %s", filename)

		file, err := ioutil.ReadFile(filename)

		if err != nil {
			log.Printf("Cannot open %s: %s", filename, err)
			continue
		}

		// try to apply the confing
		log.Printf("Applying %s", filename)
		err = config.ApplyConfig(file)

		if err != nil {
			log.Printf("Error applying %s (probably invalid config file): %s", filename, err)
			continue
		}
	}

	// TODO: actually count how much config files we applied
	log.Println("Applied all configuration files")
}
